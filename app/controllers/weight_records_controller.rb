class WeightRecordsController < ApplicationController
  protect_from_forgery except: [:upload_weight]

  def index
    @weight_records = WeightRecord.all.order("created_at desc")

    return render template: "/weight_records/_records", locals: {weight_records: @weight_records}, layout: false if request.xhr?
  end

  def create
    @weight_record = WeightRecord.new(customer_info: params[:customer_info])

    if @weight_record.save
      render template: "/weight_records/_record", locals: {weight_record: @weight_record}, layout: false
    else
      render text: @weight_record.errors.messages[:customer_info].first, status: 400
    end
  end

  def upload_weight
    @weight_record = WeightRecord.last

    if @weight_record && @weight_record.weight.nil?
      @weight_record.update(weight: params[:weight])
    end

    render json: {message: 'success'}, status: 200
  end
end
