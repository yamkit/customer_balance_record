Rails.application.routes.draw do
  resources :weight_records, only: [:index, :create] do
    collection do
      post :upload_weight
    end
  end

  root 'weight_records#index'
end
